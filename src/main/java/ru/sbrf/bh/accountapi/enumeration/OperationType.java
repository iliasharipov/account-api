package ru.sbrf.bh.accountapi.enumeration;

public enum OperationType {
    INCREASE,
    REDUCE
}
